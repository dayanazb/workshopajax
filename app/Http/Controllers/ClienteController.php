<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $clientes = DB::table('clientes')->orderBy('id', 'desc')->paginate(10);
        return view('clientes')->with('clientes', $clientes);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = DB::table('clientes')->orderBy('id', 'desc')->paginate(10);
        return view('crear_cliente')->with('clientes', $clientes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $c = new Cliente;
        $c->cedula = $request->cedula;
        $c->nombre = $request->nombre;
        $c->apellido = $request->apellido;
        $c->telefono = $request->telefono;
        if ($c->save()) {
          return redirect()->action('ClienteController@index');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $c = Cliente::find($id);
        return view('cliente')->with('cliente', $c);
    }

    /**
     * Show the form for editing the specified resource.
     *  In this case is used to update the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente=Cliente::find($id);
        $clientes = DB::table('clientes')->orderBy('id', 'desc')->paginate(10);
        return view('editar_cliente')->with(['cliente' => $cliente, 'clientes' => $clientes]);
    }

    public function update(Request $request, $id){
        DB::table('clientes')->where('id', $id)->update(['cedula' => $request->cedula, 'nombre' => $request->nombre, 'apellido' => $request->apellido, 'telefono' => $request->telefono]);
        $clientes = DB::table('clientes')->orderBy('id', 'desc')->paginate(10);
        return view('clientes')->with('clientes', $clientes);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        DB::table('clientes')->where('id', $id)->delete();
        return response()->json(['code' => 200]);
    }


    public function addItem(Request $request) {
    $clientes = array (
            'cedula' => 'required',
            'nombre' => 'required',
            'apellido' => 'required',
            'telefono' => 'required'
    );
    $validator = Validator::make ( Input::all (), $clientes );
    if ($validator->fails ())
        return Response::json ( array (
                    
                'errors' => $validator->getMessageBag ()->toArray ()
        ) );
        else {
            $data = new Data ();
            $data->cedula = $request->cedula;
            $data->nombre = $request->nombre;
            $data->apellido = $request->apellido;
            $data->telefono = $request->telefono;
            $data->save ();
            return response ()->json ( $data );
        }
}

}
