

@extends('layouts.app')

@section('crear_cliente')
  <div class="container">
      <div class="row justify-content-center">

          <div class="col-sm-8">
              <div class="card">
                  <div class="card-header">Formulario de Cliente</div>

                  <div class="card-body">
                      @if (session('status'))
                          <div class="alert alert-success" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif
                      <div class="form-group row add">
                          <div class="col-md-8">
                              <input type="text" class="form-control" id="cedula" cedula="cedula"
                                  placeholder="Cedula" required>
                              <p class="error text-center alert alert-danger hidden"></p>
                          </div>
                          <div class="col-md-8">
                              <input type="text" class="form-control" id="nombre" name="nombre"
                                  placeholder="Nombre" required>
                              <p class="error text-center alert alert-danger hidden"></p>
                          </div>
                          <div class="col-md-8">
                              <input type="text" class="form-control" id="apellido" name="apellido"
                                  placeholder="Apellido" required>
                              <p class="error text-center alert alert-danger hidden"></p>
                          </div>
                          <div class="col-md-8">
                              <input type="text" class="form-control" id="telefono" name="telefono"
                                  placeholder="Telefono" required>
                              <p class="error text-center alert alert-danger hidden"></p>
                          </div>
                          <div class="col-md-4">
                              <button class="btn btn-primary" type="submit" class="add">
                                  <span class="glyphicon glyphicon-plus"></span> ADD
                              </button>
                          </div>
                      </div>

                  </div>
              </div>
          </div>

      </div>
  </div>
<script src="{{ asset('js/cliente.js') }}" defer></script>
@endsection




