@extends('layouts.app')

@section('clientes')
    <div class="container">
        {!! Form::open(['url' => '/cliente/'.$cliente->id, 'method' => 'put']) !!}

        <div class="justify-content-center">
        	<div class="row">
        		<div class="col-md-2"></div>
          		<div class="col-md-8">
            		<div class="card">
	              		<h2>Editar cliente</h2>
              			<div class="card-header">
                			<div class="row">
                  				<div class="col-md-8">

                  					<div class="row">
							          <div class="col-md-4"></div>
							         	<div class="form-group col-md-4">
							            	<label for="cedula">Cedula:</label>
							            	<input type="text" class="form-control" name="cedula" value="{{$cliente->cedula}}">
							          	</div>
							          	<div class="form-group col-md-4">
							            	<label for="nombre">Nombre:</label>
							            	<input type="text" class="form-control" name="nombre" value="{{$cliente->nombre}}">
							          	</div>
							          	<div class="form-group col-md-4">
							            	<label for="apellido">Apellido:</label>
							            	<input type="text" class="form-control" name="apellido" value="{{$cliente->apellido}}">
							          	</div>
							          	<div class="form-group col-md-4">
							            	<label for="telefono">Telefono:</label>
							            	<input type="text" class="form-control" name="telefono" value="{{$cliente->telefono}}">
							          	</div>
							        </div>

							        <div class="row">
							          <div class="col-md-4"></div>
							          <div class="form-group col-md-4">
							            <button type="submit" class="btn btn-success">Actualizar</button>
							          </div>
							        </div>

                  				</div>
                  			</div>
                  		</div>
                  	</div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
    </div>
@endsection