$(document).ready(function(){

});

var Cliente = {
	delete: function(cliente_id){
    return $.getJSON('/cliente/'+cliente_id+'/delete');
  },
}

$('.a_delete').on("click",function(){
	
    var confirmLeave = confirm('¿Seguro que desea eliminar categoría?');
	if (confirmLeave==true)
	{
  		var cliente_id = $(this).attr("id");
  		Cliente.delete(cliente_id).done(function(json){
  			if (json.code == 200) {
  				location.reload();
  			}
  		});
	}
});




$(".add").click(function() {
$.ajax({
        type: 'post',
        url: '/addItem',
        data: {
            '_token': $('input[name=_token]').val(),
            'cedula': $('input[cedula=cedula]').val(),
            'nombre': $('input[nombre=nombre]').val(),
            'apellido': $('input[apellido=apellido]').val(),
            'telefono': $('input[telefono=telefono]').val()
        },
        success: function(data) {
            if ((data.errors)) {
                $('.error').removeClass('hidden');
                $('.error').text(data.errors.cedula);
                $('.error').text(data.errors.nombre);
                $('.error').text(data.errors.apellido);
                $('.error').text(data.errors.telefono);
            } else {
                $('.error').remove();
                $('#table').append("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.cedula + "</td><td>" + data.nombre + "</td><td>" + data.apellido + "</td><td>" + data.telefono + "</td><td><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-cedula='" + data.cedula + "' data-nombre='" + data.nombre + "' data-apellido='" + data.apellido + "' data-telefono='" + data.telefono + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' ddata-id='" + data.id + "' data-cedula='" + data.cedula + "' data-nombre='" + data.nombre + "' data-apellido='" + data.apellido + "' data-telefono='" + data.telefono + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
            }
        },
    });
    $('#cedula').val('');
    $('#nombre').val('');
    $('#apellido').val('');
    $('#telefono').val('');
});