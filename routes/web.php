<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('category','CategoryController');
Route::get('list_categories','CategoryController@index_categories');
Route::get('/view_products/{id}','CategoryController@getProducts');
Route::get('/category/{id}/delete', 'CategoryController@delete');
Route::resource('product','ProductController');
Route::get('/product/{id}/delete', 'ProductController@delete');

Route::resource('cliente','ClienteController');
Route::get('/cliente/{id}/delete', 'ClienteController@delete');

Route::post ( '/addItem', 'ClienteController@addItem' );